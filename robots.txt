User-agent: *
Disallow: /css/
Disallow: /js/
Disallow: /img/top/
Disallow: /plugin/
Disallow: /rs-plugin/
Disallow: /_lib/

Sitemap: http://www.rebelcode.cl/sitemap.xml